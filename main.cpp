/**************************************************************************//**
* @file
* @brief Encodes and decodes files using Huffman encoding
*
* @mainpage Huffman Encoding
*
* @section course_section Course Information
*
* @author Erica Keeble
* @author Seth Snyder
*
* @date 10/21/18
*
* @par Instructor:
*       Professor Mengyu Qiao
*
* @par Course:
*       CSC 315 - 10:00am section
*
* @section program_section Program Information
*
* @details This program takes in 3 arguments from the command line, which
* describe whether to encode or decode the file, the input file, and the  
* output file name. When enoding, the program creates a frequency histogram 
* out of the characters in the input file. Out of that histogram, it creates 
* a Huffman encoding tree, from which it extracts the codes for each 
* character. It uses these codes to encrypt original text from the input file 
* and stores the encryption in the output, as well as stores the huffman tree. 
* When decoding, the program takes the input file which is encoded, extracts
* the huffman tree, which it uses to decode the file and store in an output
* file. 
*
* @section compile_section Compiling and Usage
*
* @par Compiling Instructions:
*               Command line arguments need to define whether to encode or 
*		decode, declare the input file name, and the output file
*		name. 
*
* @par Usage
* @verbatim
* c:\> huffman -e input.file encoded.txt
* c:\> huffman -d encoded.file decoded.txt
* @endverbatim
*
* @section todos_bugs_modification_section ToDo, Bugs, and Modifications
* @bug The decoded file has an extra E that shouldnt be there im not sure
* how this is happening.
*
* @par Modification and Development Timeline:
*   <a href="https://gitlab.mcs.sdsmt.edu/7431673/csc315_fall2018_program1
/commits/master">Click here for commit log.</a>
*
*******************************************************************************/

#include "header.h"

/**************************************************************************//**
* @author Seth Snyder
* @author Erica Keeble
*
* @par Description:
* Main
*
* @param[in]       argc - number of arguments in the command line
* @param[in]       argv[] - array containing command line arguments
*
* @returns 0 if successful 
******************************************************************************/
int main(int argc, char *argv[])
{

priority_queue<hist, vector<hist>, comparison> histHeap;

	//if we are encoding a file, do this
	if(argv[1][1] == 'e')
	{
	encode(argv[2], argv[3], histHeap);
	}
	//if we are decoding a file, do this
	else if(argv[1][1] == 'd')
	{
	decode(argv[2], argv[3], histHeap);
	}
	else
	{
		cout << "Incorrect Command Line" << endl;
		cout << "To encode: huffman -e originalfile encodedfile"<< endl;
		cout << "To decode: huffman -d encodedfile decodedfile"<< endl; 
	}
	
	return 0;
}
