#include "header.h"

//the function that finds the frequencies of certain letters and stores them in a list

/**************************************************************************//**
* @author Seth Snyder
*
* @par Description:
* This function finds the frequency of the given characters and puts them
* into a priority queue so that the tree can be made out of them
*
* @param[in]	input - vector with all text from input file 
* @param[in]	histHeap - an instance of our tree
*
******************************************************************************/
void findfrequencies(vector<char> input, minheap &histHeap)
{
   int freq = 0;

   sort(input.begin(), input.end());

   char val = input[0];

//loop that does the magic

   for(int i = 0; i <= input.size(); i++)
   {
      if(input[i] != val)
      {
	hist *h1;
	h1 = new(nothrow) hist;
         h1->value = val;
         h1->freq = freq;
	 h1->l = NULL;
	 h1->r = NULL;
         histHeap.push(*h1);
         val = input[i];
         freq = 0;
	}
	

      freq = freq + 1;
   }

}

//function to output the data from the codes vector

/**************************************************************************//**
* @author Seth Snyder
*
* @par Description:
* This function encodes the given input file into the smaller binary form by  
* using the tree, codes function, and bitwise operators.
*
* @param[in]	histHeap - an instance of our tree
* @param[in]	ifilename - name of the encoded file that will be opened
* @param[in]	efilename - name of the decoded file that will be opened
*
* @returns true if file is opened and read from
* @returns false otherwise
*
******************************************************************************/
bool encode(string ifilename, string efilename, minheap histHeap)
{

vector<char> input;
vector<vector<bool> > codes;
hist *hfmtree;
char v;
unsigned char data = 0;
int f;
int val;
int count = 0;
int size = 0;

int numcodedigits, bEncoded=0, bHistogram=0, origNumBytes=0, numcodes;
double entropy;

//read in the chars form the input file
readFile(input, ifilename);

//find the frequencies and put the found values into nodes in a queue
findfrequencies(input, histHeap);

//makes the huffman tree and returns a pointer to the top node
hfmtree = makeHfmTree(histHeap);

//uses the huffman tree to generate a 2d vector with the bool values for 
//each letter in it
codes = findCodes(hfmtree);

size = histHeap.size();
minheap heapcpy = histHeap;
origNumBytes = input.size();
numcodes = histHeap.size() - 1;

	//open output file
	ofstream fout (efilename.c_str(), ios::binary);
	if(!fout)
	{
		cout << "Output file did not open." << endl;
		return false;
	}
	//output the histogram to the file
	//repeat unti histHeap is empty

	fout.write((char *) &size, sizeof(int));
	while(histHeap.size() >= 1)
	{


		//pop top element off histHeap
		hist * h2;
		h2 = new(nothrow) hist;
		*h2 = histHeap.top();
		histHeap.pop();
		v = h2->value;
		f = h2->freq;

		//writing the heap to the file
		//I think i may have to switch the order they are in so i can read 
		//correctly in decode
		fout.write((char *) &v, sizeof(char));
		fout.write((char *) &f, sizeof(int));

		
		bHistogram = bHistogram + 3;

	}
	//output the encoded values to the file

	for(int i = 0; i != input.size(); i++)
	{

		val = (int)input[i];

		for(int j =0; j != codes[val].size(); j++)
		{
			data = data | codes[val][j];
			
		
			if(count >= 7)
			{
			fout.write((char *) &data, sizeof(char));
			count = -1;
			data = 0;
			bEncoded++;
			}

			data <<= 1;
			count++;


		}

	}
	if(count <= 7 && count != 0)
	{
	data <<= (7 - count);
	fout.write((char *) &data, sizeof(char));
	}

	fout.close();

//output data to the screen
numcodedigits = screenE1(heapcpy, hfmtree, input, codes, ifilename, entropy);

//output more data to the screen
screenE2(bEncoded, efilename, bHistogram, origNumBytes, codes, numcodedigits, 
    numcodes, entropy); 


	

	return true;


}

//function to decode a given coded file and output it as a decoded file

/**************************************************************************//**
* @author Seth Snyder
*
* @par Description:
* Decodes the encoded file using some bitwise operators and using that to traverse
* the tree to find the right node and output the data.
*
* @param[in]	histheap - an instance of our tree
* @param[in]	efilename - name of the encoded file that will be opened
* @param[in]	dfilename - name of the decoded file that will be opened
*
* @returns true if file is opened and read from
* @returns false otherwise
*
******************************************************************************/
bool decode(string efilename, string dfilename, minheap histheap)
{
char v;
unsigned char temp = 0;
unsigned char temp2 = 0;
int num = 128;
unsigned char thing;
thing = (char)num;
int bit;
int f = 0;
int size = 0;
hist *h1;
hist *hfmtree;
vector<vector<bool> > codes;
vector<char> edata;
int bDecoded, bHistogram, bWritten;

	//open the encoded file
	ifstream fin (efilename.c_str(), ios::binary);		
	if(!fin)
	{	
	cout << "Input file did not open" << endl;
	return false;
	}
	
	//open the file you are going to decode to
	ofstream fout (dfilename.c_str(), ios::binary);
	if(!fout)
	{
		cout << "Output file did not open." << endl;
		return false;
	}
	fin.read((char *) &size, sizeof(int));
	
	//rebuild the histogram
	for(int k = 0; k != size; k++)
	{
	hist * h2;
	h2 = new(nothrow) hist;
	fin.read(&v, sizeof(char));
        h2->value = v;
	fin.read((char *) &f, sizeof(int));
        h2->freq = f;
        histheap.push(*h2);
	
	bHistogram++;
  	}

	//remake the huffman tree
	hfmtree = makeHfmTree(histheap);
	h1 = hfmtree;
	//compare the incoming data with the codes vector to see what 
        //letter it corresponds to --- better way to do it?
	while(!fin.eof())
	{
		fin.read((char*)&temp2, sizeof(char));
		bDecoded++;

		for(int i = 0; i <= 7; i++)
		{ 
			temp = temp2;
			bit = temp & thing;

			if(h1->l == NULL && h1->r == NULL && h1->value != '\0')
			{
				if(h1->value == '$')
				{
					screenD(bDecoded, efilename, bHistogram, bWritten, dfilename);
	
					fin.close();
					fout.close();
					return true;
				}
				else
				{	
					fout.write((char*) &h1->value, sizeof(char));
					h1 = hfmtree;
					bWritten++;
				}
					i--;
			}
			else if(bit == 128)
			{
				h1 = h1->r;
				temp2 <<= 1;
			}
			else if(bit == 0)
			{
				h1 = h1->l;
				temp2 <<= 1;
			}
		}


	}

	screenD(bDecoded, efilename, bHistogram, bWritten, dfilename);
	
	fin.close();
	fout.close();
	return true;
	
}

