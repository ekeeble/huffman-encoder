/*************************************************************************//**
 * @file
 * @brief  Erica's functions, with declarations in header.h
 *****************************************************************************/
#include "header.h"

#include <iostream>
using namespace std;


/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Creates Huffman tree using histogram of input file character frequencies.  
*
* @param[in]       histHeap - the min-heap of input file character frequencies 
*
* @returns pointer to first node in Huffman tree
*
******************************************************************************/
hist* makeHfmTree(minheap histHeap)
{
	hist *hfmtree;
	hist *h1, *h2;
	h1 = new(nothrow) hist;
	h2 = new(nothrow) hist;

	//repeat unti histHeap is empty
	while(histHeap.size() > 1)
	{		
		hist *h3, *h4;
		h3 = new(nothrow) hist;
		h4 = new(nothrow) hist;
		//pop top element off histHeap
		*h1 = histHeap.top();
		histHeap.pop(); 
		h3->r = h1->r;
		h3->l = h1->l;
		h3->value = h1->value;
		h3->freq = h1->freq;
		
		//pop next top element off histHeap
		*h2 = histHeap.top();
		histHeap.pop();
		h4->r = h2->r;
		h4->l = h2->l;
		h4->value = h2->value;
		h4->freq = h2->freq;

		//make a new node, with \0, and sum of two elements.
		hist *newN;
		newN = new(nothrow) hist;
		newN->value = '\0';
		newN->freq = h3->freq + h4->freq;
		newN->l = h3;
		newN->r = h4;
	
		//push that new node onto the heap
		histHeap.push(*newN);
	}

	*h1 = histHeap.top();
	hfmtree = h1;		//hfmtree points at first element of tree

	return hfmtree;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Uses the Huffman tree to find the codes for all the characters from the
* input file. Stores all those codes in a vector of boolean vectors.
*
* @param[in]       hfmtree - The huffman tree
*
* @returns codes that correspond with each character in a vector<vector<bool> 
* @returns false otherwise
*
******************************************************************************/
vector< vector<bool> > findCodes(hist *hfmtree)
{
	vector< vector<bool> > codes;
	codes.resize(256);

	stack<bool> pathStk;			//pathRvspy is reverse copy
	
	//depth-first search of tree
	treeTraversal(hfmtree, pathStk, codes);	
	
	return codes;
}


/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Recursive function that traverses the Huffman tree. As it does a depth-first-
* seach, it keeps track of the path down the tree with 1s and 0s on a stack.
* When it reaches a valid leaf node, it adds that path to the codes vector 
* at the index that is the ascii value of the corresponding character. When
* it returns from visiting a node, it will delete it. 
*
* @param[in]       node - this is the node that we are looking
* @param[in]	   pathStk - the stack that holds the path of 0s and 1s
* @param[out]	   codes - stores codes that correspond with each char in tree 
*
******************************************************************************/
void treeTraversal(hist *node, stack<bool> pathStk, vector <vector<bool> > 
    &codes)
{
int val =0;
bool path;

	//if you can go left, go left. Add 0 to pathStk. Delete the node upon return
	if(node->l != NULL)
	{
		pathStk.push(0);
		treeTraversal(node->l, pathStk, codes);
		delete(node->l);		//THIS IS PROBLY WRONG WAY TO DELETE NODE
		pathStk.pop();			//pop off last path entry after we return
	}	
	//if you can't go left, but you can go right, go right, add 1 to path stack
	if(node->r != NULL)
	{	
		pathStk.push(1);
		treeTraversal(node->r, pathStk, codes);
		delete(node->r);
		pathStk.pop();		//pop it off after we return	
	}	
	//if you can't go either, youre at a leaf. Check for '\0'. do leaf things.
	if(node->l == NULL && node->r == NULL && node->value != '\0')
	{
		stack<bool> pathStkcpy (pathStk), pathRvcpy;//pathRvspy is reverse copy
		
		//when node is reached, push the stack into a vector at index "ascii"
		//reverse the stack's order so we can store it the correct orientation
		while(!pathStkcpy.empty())
		{
			pathRvcpy.push(pathStkcpy.top());
			pathStkcpy.pop();
		}

		//stores stack (path) in the codes vector at the index that is the ascii 
		//value of the character
		while(!pathRvcpy.empty())
		{	
			path = pathRvcpy.top();

			val = node->value;

			codes[val].push_back(path);
			pathRvcpy.pop();
		}
	
		return;
	}	
	//if the end node is not one that has a letter, return
	else
		return;
}
		

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Reads characters from file into the vector "inFileData"
*
* @param[in]	fileData_input - vector with all text from input file 
* @param[in]	fileName - name of the input file that will be opened
*
* @returns true if file is opened and read from
* @returns false otherwise
*
******************************************************************************/
bool readFile(vector<char> &fileData_input, string fileName)
{	
	char c;

	ifstream fin (fileName.c_str(), ios::binary);		
	if(!fin)
	{
		cout << "Input file did not open" << endl;
		return false;
	}

	while(!fin.eof())
	{
		fin.read(&c, sizeof(char));	//read in 1 char from the file
		fileData_input.push_back(c); 	//push back dereferenced character
	}
	fileData_input.push_back('$'); 
	fin.close();
	return true;
}



/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Outputs encoding data to the screen
*
* @param[in]	histHeap - min heap with all hist nodes 
* @param[in]	hfmtree - huffman tree with all the nodes and such
* @param[in]	input - contains all the characters from the input file 
* @param[in]	codes - all codes for the characters in the tree
* @param[in]	infile - input file name 
* @param[in]	entropy - the entropy that we calculate and use in the next
*
* @returns number of code digits 
*
******************************************************************************/
int screenE1(minheap histHeap, hist* hfmtree, vector<char> input, 
    vector<vector <bool> > codes, string infile, double& entropy)
{
	cout << "Huffman Encoder Pass 1" << endl << "--------------------" << endl;
	cout << "Read " << input.size() << " bytes from " << infile << ", "
		 << "found " << histHeap.size()-1 <<  " code words" << endl << endl;


	cout << "Huffman Code Table" << endl << "----------------------" << endl;
	cout << setw(10) << "ASCII Code" << setw(25) << "Probability (%)" << setw(18)
		 << "Huffman Code" << endl; 

	int size = histHeap.size();
	double freq, insize, percent;
	int freq_i, insize_i;
	int numcodes = 0;
	double prob;
	double log = 0;

	//print nodes
	for(int i = 0; i < size; i++)
	{

		hist *h;
		*h = histHeap.top();
		prob = (double)h->freq/input.size();

		cout << setw(4) << (int)h->value << "( " << h->value 
			<< " )"	<< setw(19)	<< setprecision(2) << fixed << prob*100 
			<< "%" << setw(17);
		for(int j = 0; j < codes[h->value].size(); j++)
		{
			cout << codes[h->value][j];
			numcodes++;
		}
		histHeap.pop();
		cout << endl;

		if(prob != 0)	
		{	log = log2(prob);
			entropy += prob * log;		
		}
	}

	
	cout << endl;
	return numcodes;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Outputs encoding data to the screen
*
* @param[in]	bEncoded - number of bytes written to the encoded file 
* @param[in]	outfile - name of the output file
* @param[in]	bHistogram - number of bytes to write histogram to encoded file 
* @param[in]	origNumBytes - number of bytes in the input file
* @param[in]	codes - data structure that holds codes with their ascii values
* @param[in]	numcodedigits - total number of code digits
* @param[in]	numcodes - how many codes	
* @param[in]	entropy - calculated in other function
*
******************************************************************************/
void screenE2(int bEncoded, string outfile, int bHistogram, int origNumBytes,
	 vector<vector <bool> > codes, int numcodedigits, int numcodes, double entropy)
{

	cout << "Huffman Encoder Pass 2" << endl << "--------------------" << endl;
	cout << "Wrote " << bEncoded << " bytes to " << outfile 
		 << " (" << bHistogram + bEncoded << " bytes including histogram)" 
		 << endl << endl;
	
	cout << "Huffman Coding Statistics" << endl << "--------------------" << endl;
	//calculate compression ratio - compressed size / original size
	cout << "Compression ratio = " << setprecision(2) << fixed 
	  	 << ((double)bEncoded/origNumBytes*100) << "%"  << endl;	

	//calculate entropy
	cout << "Entropy = " << -entropy  << endl;			

	//average bits
	cout << "Average bits per symbol in Huffman coding = " << setprecision(2) 
		 << (double)numcodedigits/numcodes << endl;	
	
} 


/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Outputs decoding data to the screen
*
* @param[in]	bDecoded - number of bytes read from encoded file 
* @param[in]	infile - name of the input encoded file
* @param[in]	bHistogram - number of bytes to read histogram from encoded file 
* @param[in]	bWritten - number of bytes written to decoded file	
* @param[in]	outfile - name of the decoded file	
*
******************************************************************************/
void screenD(int bDecoded, string infile, int bHistogram, int bWritten, 
    string outfile)
{
	cout << "Huffman Decoder" << endl << "----------------------" << endl;

	cout << "Read " << bDecoded << " from " << infile << " (" 
	    << bHistogram+bDecoded << " bytes including histogram)" << endl;
	
	cout << "Wrote " << bWritten << " decoded bytes to " << outfile << endl;

	cout << "Compression ratio: " << setprecision(2) << fixed 
	     << ((double)bDecoded/bWritten)*100 << "%" << endl;

}
