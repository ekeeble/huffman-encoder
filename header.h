/*************************************************************************//**
 * @file
 * @brief Header file containing the includes for this program, function
 * declarations from erica.cpp and seth.cpp, the hist struct, and the
 * minheap priority queue
 * 
 *****************************************************************************/
#include <iomanip>
#include <list>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <queue>
#include <stack>
#include <cmath>

using namespace std;
#ifndef _HEADER_H
#define _HEADER_H

/*!
*@brief The structure used to store the histogram value, frequency, 
* left ptr, and right ptr
*/
struct hist
{
   char value;	/**< letter value in the hist node*/
   int freq; 	/**< frequency of the value */
   hist *l;		/**< left pointer */
   hist *r;		/**< right pointer */
};

/*!
*@brief Comparator used in the min heap type declaration from priority_queue
*/
class comparison
{
public:
	/***********************************************************************//**
	* @author Erica Keeble
	*
	* @par Description:
	*   Comparator used in declaration of priority_queue
	*
	* @param[in]      n1 - node to compare 
	* @param[in]      n2 - node to compare
	*
	* @returns true if n1 is greater than n2
	* @returns false otherwise
	***************************************************************************/	
	bool operator() (const hist& n1, const hist& n2)
	{
		return n1.freq > n2.freq;
	}
};


/** Min heap type, called minheap. Its a priority queue that stores hist nodes */
typedef priority_queue<hist, vector<hist>, comparison> minheap;


bool readFile(vector<char> &inFileData, string fileName);

bool encode(string ifilename, string efilename, minheap histHeap);

bool decode(string efilename, string dfilename, minheap histheap);

void findfrequencies(vector<char> input, minheap &histHeap);

hist* makeHfmTree(minheap histHeap);

vector< vector<bool> > findCodes(hist* hfmtree);

void treeTraversal(hist* node, stack<bool> pathStk, vector< vector<bool> > &codes);

int screenE1(minheap histHeap, hist* hfmtree, vector<char> input, 
    vector<vector <bool> > codes, string infile, double& entropy);

void screenE2(int bEncoded, string outfile, int bHistogram, int origNumBytes, 
    vector<vector <bool> > codes, int numcodedigits, int numcodes, double entropy);

void screenD(int bDecoded, string infile, int bHistogram, int bWritten, 
    string outfile);

#endif
